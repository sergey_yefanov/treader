//
//  TRFileManager.swift
//  TReader
//
//  Created by Sergey Yefanov on 15.02.15.
//  Copyright (c) 2015 Sergey Yefanov. All rights reserved.
//

private let DOCUMENTS = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true).last as String

import UIKit

private let singleton = TRFileManager()

class TRFileManager: NSObject, TRParserDelegate
{
    class var sharedInstance : TRFileManager
    {
        return singleton
    }
    
    var zip : ALZipArchive!
    
    func numberOfItems() -> Int
    {
        return parcedItems()!.count
    }
    
    func parcedItems() -> [AnyObject]?
    {
        let directoryItems = NSFileManager.defaultManager().contentsOfDirectoryAtURL(pathForParsedBooks(),
                            includingPropertiesForKeys: [NSURLNameKey] ,
                            options: NSDirectoryEnumerationOptions.SkipsSubdirectoryDescendants,
                            error: nil)
        
        let arr = NSArray(array: directoryItems!)
        
        var objects = NSMutableArray()
        
        for url in arr
        {
            var isDirectory: AnyObject?
            url.getResourceValue(&isDirectory, forKey: NSURLIsDirectoryKey, error: nil)
            
            var name: AnyObject?
            url.getResourceValue(&name, forKey: NSURLNameKey, error: nil)
            
            let ext = name?.componentsSeparatedByString(".").last as String
            
            if isDirectory?.boolValue == false && ext == "plist"
            {
                objects.addObject(url)
            }
        }
        
        println(objects)
        return objects
    }
    
    func addItem(#path: NSURL)
    {
        let fullDocumentName = path.path?.lastPathComponent
        var unzippingName = fullDocumentName?.stringByDeletingPathExtension
        let arr = unzippingName!.componentsSeparatedByString("-")
        
        var unzippingName1 = NSMutableString()
        
        if (arr.count - 1 > 0)
        {
            for var i = 0; i < arr.count - 1; i++
            {
                unzippingName1.appendString(arr[i])
            }
            
            unzippingName = unzippingName1
        }
        
        let extensionType = fullDocumentName?.componentsSeparatedByString(".").last
        let documentName = fullDocumentName?.componentsSeparatedByString(".").first
        let folderURL = path.URLByDeletingLastPathComponent!
        
        TRParser.sharedInstance.delegate = self
        zip = ALZipArchive()
        
        if extensionType == "zip"
        {
            if zip.unzipOpenFile(path.path) == true
            {
                if zip.unzipFileTo(pathForParsedBooks().URLByAppendingPathComponent("zip").path!, overWrite: true) == true
                {
                    TRParser.sharedInstance.parse(url: pathForParsedBooks().URLByAppendingPathComponent("zip").URLByAppendingPathComponent(unzippingName!))
                    NSFileManager.defaultManager().removeItemAtPath(path.path!.stringByDeletingLastPathComponent, error: nil)
                }
                else
                {
                    println("Не могу распаковать файл")
                }
                
            }
            else
            {
                println("Не могу открыть файл")
            }
        }
        else if extensionType == "fb2"
        {
            TRParser.sharedInstance.parse(url: folderURL.URLByAppendingPathComponent(fullDocumentName!))
        }
    }
    
    func titleForItemAtIndex(Index: Int) -> String?
    {
        return (parcedItems()![Index]).lastPathComponent
    }
    
    func pathForParsedItemAtIndex(Index: Int) -> NSURL?
    {
        return NSURL(string: DOCUMENTS)
    }
    
    func removeItemAtIndex(Index: Int)
    {
        
    }
    
    func pathForParsedBooks() -> NSURL
    {
        return NSURL(string: DOCUMENTS)!
    }
    
    func itemAtIndex(Index: Int) -> NSArray?
    {
        let url = parcedItems()![Index] as NSURL
        return NSArray(contentsOfURL: url)
    }
    
    //MARK: TRParserDelegate
    
    func parseDidFinish(var #url: NSURL)
    {
        NSNotificationCenter.defaultCenter().postNotificationName("PARSE_DID_FINISH", object: nil)
        var error : NSError? = nil
        url = url.URLByDeletingLastPathComponent!
        let success = NSFileManager.defaultManager().removeItemAtPath(url.path!, error: &error)
        
        if success != true
        {
            println(error)
        }
        else
        {
            println("оригинал успешно удален")
        }
    }
    
    func readBook()
    {
//        let bookPath = NSBundle.mainBundle().pathForResource("book", ofType: "plist")
//        
//        let bookArray = NSArray(contentsOfFile: bookPath)
//        println(bookArray)
//        
//        let bookArray = NSArray(contentsOfFile: bookPath!)
//        println(bookArray)
    }
}
