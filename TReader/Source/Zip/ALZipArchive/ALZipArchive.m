//
//  ALZipArchive.m
//  ALZipArchive
//
//  Created by Artem Loenko on 4/27/13.
//  Copyright (c) 2013 Artem Loenko. All rights reserved.
//

#import "ALZipArchive.h"

#import "zlib.h"
#import "zconf.h"

#define ALNaturalLanguageDate @"01/01/1980"

@interface ALZipArchive ()

@property (nonatomic) zipFile currentZipFile;
@property (nonatomic) unzFile currentUnzFile;
@property (nonatomic, strong) NSString *currentPassword;

@end

@implementation ALZipArchive

- (id)init
{
    self = [super init];
    if (self) {}
    return self;
}

- (void)dealloc
{
	[self closeZipFile];
}

- (BOOL)createZipFile:(NSString *)zipFile;
{
	self.currentZipFile = zipOpen((const char *)[zipFile UTF8String], 0);

	if (self.currentZipFile)
	{
		return YES;
	}
	
	return NO;
}

- (BOOL)createZipFile:(NSString *)zipFile password:(NSString *)password;
{
	self.currentPassword = password;
	return [self createZipFile:zipFile];
}

- (BOOL)addFileToZip:(NSString *)file newName:(NSString *)newName;
{
	if (!self.currentZipFile)
	{
		return NO;
	}
	
	if (!newName)
	{
		newName = [file lastPathComponent];
	}

	time_t current;
	time (&current);
	
	zip_fileinfo zipInfo = {0};
	zipInfo.dosDate = (unsigned long)current;
	
	NSError *error = nil;
	NSDictionary *attributes = [[NSFileManager defaultManager] attributesOfItemAtPath:file error:&error];
	if(error == nil && attributes)
	{
		NSDate* fileDate = (NSDate *)[attributes objectForKey:NSFileModificationDate];
		if(fileDate)
		{
#if TARGET_OS_IPHONE
			zipInfo.dosDate = [fileDate timeIntervalSinceDate:[NSDate dateWithTimeIntervalSince1970:0]];
#else
			zipInfo.dosDate = [fileDate timeIntervalSinceDate:[NSDate dateWithNaturalLanguageString:ALNaturalLanguageDate]];
#endif
		}
	}
	else
	{
		NSLog(@"ERROR: %s '%@'", __PRETTY_FUNCTION__, error);
		return NO;
	}
	
	int result;
	NSData *data = nil;
	if (![self.currentPassword length] || [self.currentPassword length] == 0)
	{
		result =
			zipOpenNewFileInZip(
				self.currentZipFile,
				(const char *)[newName UTF8String],
				&zipInfo,
				NULL,
				0, //size_extrafield_local
				NULL,
				0, //size_extrafield_global
				NULL,
				Z_DEFLATED,
				Z_DEFAULT_COMPRESSION
			);
	}
	else
	{
		data = [NSData dataWithContentsOfFile:file];
		uLong crcValue = crc32(0L, NULL, 0L);
		crcValue = crc32(crcValue, (const Bytef *)[data bytes], (unsigned int)[data length]);
		result =
			zipOpenNewFileInZip3(
				self.currentZipFile,
				(const char *)[newName UTF8String],
				&zipInfo,
				NULL,
				0,	//size_extrafield_local
				NULL,
				0,	//size_extrafield_global
				NULL,
				Z_DEFLATED,
				Z_DEFAULT_COMPRESSION,
				0, 	//raw
				15, //windowBits
				8, 	//memLevel
				Z_DEFAULT_STRATEGY,
				[self.currentPassword cStringUsingEncoding:NSASCIIStringEncoding],
				crcValue
			);
	}
	
	if (result != Z_OK)
	{
		return NO;
	}
	
	if (data == NULL)
	{
		data = [NSData dataWithContentsOfFile:file];
	}
	
	unsigned int dataLen = (unsigned int)[data length];
	result = zipWriteInFileInZip(self.currentZipFile, (const void *)[data bytes], dataLen);

	if(result != Z_OK)
	{
		return NO;
	}
	
	result = zipCloseFileInZip(self.currentZipFile);
	if(result != Z_OK)
	{
		return NO;
	}
	
	return YES;
}

- (BOOL)closeZipFile;
{
	self.currentPassword = NULL;
	
	if(self.currentZipFile == NULL)
	{
		return NO;
	}
	
	BOOL result = zipClose(self.currentZipFile, NULL) == Z_OK ? YES : NO;
	self.currentZipFile = NULL;

	return result;
}

- (BOOL)unzipOpenFile:(NSString *)zipFile;
{
	self.currentUnzFile = unzOpen((const char *)[zipFile UTF8String]);
	if (self.currentUnzFile)
	{
		unz_global_info globalInfo = {0};
		if (unzGetGlobalInfo(self.currentUnzFile, &globalInfo) == UNZ_OK)
		{
			NSLog(@"%ld entries in the zip file", globalInfo.number_entry);
		}
		else
		{
			NSLog(@"ERROR: %s '%d'", __PRETTY_FUNCTION__, unzGetGlobalInfo(self.currentUnzFile, &globalInfo));
			return NO;
		}
	}
	return self.currentUnzFile != NULL;
}

- (BOOL)unzipOpenFile:(NSString *)zipFile password:(NSString *)password;
{
	self.currentPassword = password;
	return [self unzipOpenFile:zipFile];
}

- (BOOL)unzipFileTo:(NSString *)path overWrite:(BOOL)overwrite;
{
	BOOL success = YES;
	int result = unzGoToFirstFile(self.currentUnzFile);
	unsigned char buffer[4096] = {0};
	NSFileManager *fm = [NSFileManager defaultManager];
	
	if(result != UNZ_OK)
	{
		[self outputErrorMessage:@"Failed"];
		return NO;
	}
	
	do
	{
		if (!self.currentPassword || [self.currentPassword length] == 0)
		{
			result = unzOpenCurrentFile(self.currentUnzFile);
		}
		else
		{
			result = unzOpenCurrentFilePassword(self.currentUnzFile, [self.currentPassword cStringUsingEncoding:NSASCIIStringEncoding]);
		}
		
		if(result != UNZ_OK)
		{
			[self outputErrorMessage:@"Error occurs"];
			success = NO;
			break;
		}
		
		int read;
		unz_file_info fileInfo ={0};
		result = unzGetCurrentFileInfo(self.currentUnzFile, &fileInfo, NULL, 0, NULL, 0, NULL, 0);
		
		if (result != UNZ_OK)
		{
			[self outputErrorMessage:@"Error occurs while getting file info"];
			success = NO;
			unzCloseCurrentFile(self.currentUnzFile);
			break;
		}
		
		char *filename = (char *)malloc(fileInfo.size_filename + 1);
		unzGetCurrentFileInfo(self.currentUnzFile, &fileInfo, filename, fileInfo.size_filename + 1, NULL, 0, NULL, 0);
		filename[fileInfo.size_filename] = '\0';
		
		// check if it contains directory
		NSString *strPath = [NSString stringWithCString:filename encoding:NSUTF8StringEncoding];
		BOOL isDirectory = NO;
		if (filename[fileInfo.size_filename - 1] =='/' || filename[fileInfo.size_filename - 1] == '\\')
		{
			isDirectory = YES;
		}
		
		free(filename);
		
		if ([strPath rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:@"/\\"]].location != NSNotFound)
		{
			strPath = [strPath stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
		}
		
		NSString* fullPath = [path stringByAppendingPathComponent:strPath];
		
		if (isDirectory)
		{
			[fm createDirectoryAtPath:fullPath withIntermediateDirectories:YES attributes:nil error:nil];
		}
		else
		{
			[fm createDirectoryAtPath:[fullPath stringByDeletingLastPathComponent] withIntermediateDirectories:YES attributes:nil error:nil];
		}
		
		if ([fm fileExistsAtPath:fullPath] && !isDirectory && !overwrite)
		{
			if (![self overwrite:fullPath])
			{
				unzCloseCurrentFile(self.currentUnzFile);
				result = unzGoToNextFile(self.currentUnzFile);
				continue;
			}
		}
		
		FILE *fp = fopen((const char *)[fullPath UTF8String], "wb");
		while (fp)
		{
			read = unzReadCurrentFile(self.currentUnzFile, buffer, 4096);
			if (read > 0)
			{
				fwrite(buffer, read, 1, fp );
			}
			else if (read < 0)
			{
				[self outputErrorMessage:@"Failed to reading zip file"];
				success = NO;
				break;
			}
			else
			{
				break;
			}
		}
		
		if (fp)
		{
			fclose(fp);

			// set the orignal datetime property
			if (fileInfo.dosDate != 0)
			{
				NSDate *orgDate =
					[[NSDate alloc]
						initWithTimeInterval:(NSTimeInterval)fileInfo.dosDate
#if TARGET_OS_IPHONE
						sinceDate:[NSDate dateWithTimeIntervalSince1970:0]
#else
						sinceDate:[NSDate dateWithNaturalLanguageString:ALNaturalLanguageDate]
#endif
					];

				NSDictionary *attributes = [NSDictionary dictionaryWithObject:orgDate forKey:NSFileModificationDate];
				//[[NSFileManager defaultManager] fileAttributesAtPath:fullPath traverseLink:YES];
				if (attributes)
				{
					if (![[NSFileManager defaultManager] setAttributes:attributes ofItemAtPath:fullPath error:nil])
					{
						// cann't set attributes 
						NSLog(@"Failed to set attributes");
						success = NO;
					}
					
				}
			}
			
		}
		
		unzCloseCurrentFile(self.currentUnzFile);
		result = unzGoToNextFile(self.currentUnzFile);
	} while (result == UNZ_OK && UNZ_OK != UNZ_END_OF_LIST_OF_FILE);
	
	return success;
}

- (BOOL)unzipCloseFile;
{
	self.currentPassword = nil;
	if (self.currentUnzFile)
	{
		return unzClose(self.currentUnzFile) == UNZ_OK;
	}
	
	return YES;
}

#pragma mark wrapper for delegate

- (void)outputErrorMessage:(NSString *)errorMessage
{
	if (self.delegate && [self.delegate respondsToSelector:@selector(errorMessage)])
	{
		[self.delegate errorMessage:errorMessage];
	}
}

- (BOOL)overwrite:(NSString *)file
{
	if (self.delegate && [self.delegate respondsToSelector:@selector(overwriteOperation)])
	{
		return [self.delegate overwriteOperation:file];
	}
	
	return YES;
}

@end

