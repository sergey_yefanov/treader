//
//  ALZipArchive.h
//  ALZipArchive
//
//  Created by Artem Loenko on 4/27/13.
//  Copyright (c) 2013 Artem Loenko. All rights reserved.
//

#import <Foundation/Foundation.h>

#include "zip.h"
#include "unzip.h"

@protocol ALZipArchive <NSObject>
@optional
- (void)errorMessage:(NSString *)errorMessage;
- (BOOL)overwriteOperation:(NSString *)file;
@end

@interface ALZipArchive : NSObject

@property (nonatomic, retain) id delegate;

/* Create zip file at path and return YES if successed or NO if failed **/
- (BOOL)createZipFile:(NSString *)zipFile;
/* Create zip file at path with password and return YES if successed or NO if failed **/
- (BOOL)createZipFile:(NSString *)zipFile password:(NSString *)password;
/* Try to add file to current zip file and return YES if successed or NO if failed. If newName is nil used name from file param **/
- (BOOL)addFileToZip:(NSString *)file newName:(NSString *)newName;
/* Try to close zip file and return YES if successed or NO if failed **/
- (BOOL)closeZipFile;

/* Open zip file at path and return YES if successed or NO if failed **/
- (BOOL)unzipOpenFile:(NSString *)zipFile;
/* Open password protected zip file at path and return YES if successed or NO if failed **/
- (BOOL)unzipOpenFile:(NSString *)zipFile password:(NSString *)password;
/* Try to unzip file to path and return YES if successed or NO if failed (can override file at path) **/
- (BOOL)unzipFileTo:(NSString *)path overWrite:(BOOL)overwrite;
/* Try to close zip file and return YES if successed or NO if failed **/
- (BOOL)unzipCloseFile;

@end
