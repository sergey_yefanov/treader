//
//  TRParser.swift
//  TReader
//
//  Created by Sergey Yefanov on 15.02.15.
//  Copyright (c) 2015 Sergey Yefanov. All rights reserved.
//

import UIKit

private let singleton = TRParser()

protocol TRParserDelegate
{
    func parseDidFinish(#url: NSURL)
}

class TRParser: NSObject, NSXMLParserDelegate
{
    let bookArray = NSMutableArray()
    var thisKey : String = ""
    var fileName : NSMutableString!
    var delegate : TRParserDelegate? = nil
    var parser : NSXMLParser!
    var oldFileUrl : NSURL!
    
    class var sharedInstance : TRParser
    {
        return singleton
    }
    
    func parse(#url: NSURL)
    {
        oldFileUrl = url
        parser = NSXMLParser(contentsOfURL: NSURL(fileURLWithPath: url.path!))
        fileName = NSMutableString(string: url.lastPathComponent!.componentsSeparatedByString(".").first!)
        fileName.appendString(".plist")
        parser?.delegate = self
        parser?.parse()
    }
    
    func parser(parser: NSXMLParser!, didStartElement elementName: String!, namespaceURI: String!, qualifiedName qName: String!, attributes attributeDict: [NSObject : AnyObject]!)
    {
        thisKey = elementName
    }
    
    func parser(parser: NSXMLParser!, foundCharacters string: String!)
    {
        let object = [thisKey : string]
        bookArray.addObject(object)
    }
    
    func parserDidEndDocument(parser: NSXMLParser!)
    {
        let parsedPath : NSURL = TRFileManager.sharedInstance.pathForParsedBooks()
        let bookPath : NSURL = parsedPath.URLByAppendingPathComponent(fileName)
        bookArray.writeToFile(bookPath.path!, atomically: true)
        delegate?.parseDidFinish(url: oldFileUrl)
    }
}
