//
//  ViewController.swift
//  TReader
//
//  Created by Sergey Yefanov on 02.02.2015

import UIKit

class TRReaderVC: UIViewController, UIAlertViewDelegate, TRTextViewDelegate, UIPopoverPresentationControllerDelegate
{
    // MARK: - Constants
    
    let translatorManager = TRTranslatorManager()
    
    // MARK: - Variables
    var manuallyTranslatedWords = Dictionary<String, String>()
    var text: String = String()
    {
        willSet
        {
            textView?.text = newValue
        }
    }
    
    var book : NSArray?
    {
        didSet
        {
                
        }
    }
    
    // MARK: - Interface Variables
    
    @IBOutlet var textView: TRTextView?
    
    // MARK: - Initialization
    
    required init(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
    }
    
    override func viewDidAppear(animated: Bool)
    {
        super.viewDidAppear(animated)
        textView?.delegate = self
        
        let mybook: NSMutableString = NSMutableString()
        
        for dict in book!
        {
            let myDict = dict as NSDictionary
            let key = myDict.allKeys.first as String
            let value: String = myDict[key] as String
            
            mybook.appendString(value + "\n")
        }
        
        text = mybook
    }
	
    func parseToWords(text: String) -> Array<String>
    {
        var wordSet = NSMutableSet()
        
        for sentence in text.componentsSeparatedByCharactersInSet(NSCharacterSet.punctuationCharacterSet())
        {
            for word in sentence.componentsSeparatedByCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
            {
                if !word.isEmpty && !wordSet.containsObject(word)
                {
                    wordSet.addObject(word.lowercaseString)
                }
            }
        }
        
        if let wordArray = wordSet.allObjects as? Array<String>
        {
            return wordArray
        }
        
        return Array<String>()
    }
    
    // MARK: - Alert View Delegate Methods
    
    func alertView(alertView: UIAlertView!, clickedButtonAtIndex buttonIndex: Int)
    {
        if buttonIndex == 0
        {
            if let pastingText = UIPasteboard.generalPasteboard().string
            {
                text = pastingText
            }
        }
    }
    
    // MARK: - TRTextViewDelegate Methods
    
    func didTap(#word: String?, point: CGPoint?)
    {
        if let myWord = word
        {
            translatorManager.translate(word: myWord, completion: { (translates : [String]?) -> () in
                
                let storyboad = UIStoryboard(name: "Main", bundle: nil)
                let viewController = storyboad.instantiateViewControllerWithIdentifier("TRPopoverVC") as TRPopoverVC
                
                viewController.words = translates
                
                viewController.modalPresentationStyle = UIModalPresentationStyle.Popover
                viewController.popoverPresentationController!.delegate = self
                viewController.popoverPresentationController!.sourceView = self.view;
                viewController.preferredContentSize = viewController.size
                viewController.popoverPresentationController!.sourceRect = CGRect(origin: point!, size: CGSizeZero);
                
                self.presentViewController(viewController, animated: true, completion: { () -> Void in
                    
                })
                
                var strTranslates : String = ""
                
                for word in translates!
                {
                    strTranslates += word
                    strTranslates += "\n"
                }
            })
        }
    }
    
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.None
    }
}

