//
//  TRBooksVC.swift
//  TReader
//
//  Created by Sergey Yefanov on 14.02.15.
//  Copyright (c) 2015 Sergey Yefanov. All rights reserved.
//

import UIKit

class TRBooksVC: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    @IBOutlet weak var tableView : UITableView?
    
    override func viewDidLoad()
    {
        
        super.viewDidLoad()
        NSNotificationCenter.defaultCenter().addObserverForName("PARSE_DID_FINISH", object: nil, queue: nil) { note -> Void in
            self.tableView!.reloadData()
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        var cell = tableView.dequeueReusableCellWithIdentifier("MenuCell") as UITableViewCell?
        
        if (cell == nil)
        {
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "MenuCell")
        }
        
        cell?.textLabel?.text = TRFileManager.sharedInstance.titleForItemAtIndex(indexPath.row)
        
        return cell!;
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return TRFileManager.sharedInstance.numberOfItems()
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        let appDelegate = UIApplication.sharedApplication().delegate as TRAppDelegate
        let book = TRFileManager.sharedInstance.itemAtIndex(indexPath.row)
        appDelegate.menuViewController?.showBook(book!)
    }
}
