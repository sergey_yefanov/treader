//
//  TRMenuVC.swift
//  TReader
//
//  Created by Сергей Ефанов on 02.02.15.
//  Copyright (c) 2015 Sergey Yefanov. All rights reserved.
//

import UIKit

class TRMenuVC: UIViewController, UITableViewDataSource, UITableViewDelegate
{
    var menuItems : NSArray?
    let heightCell : CGFloat = 40
    
    @IBOutlet weak var tableView : UITableView?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        let menuItemsPath = NSBundle.mainBundle().pathForResource("MenuItems", ofType: "plist")
        
        if let path = menuItemsPath
        {
            menuItems = NSArray(contentsOfFile: path)
        }
        
        tableView?.backgroundColor = UIColor.clearColor()
        tableView?.separatorStyle = .None
        let inset = (CGFloat(tableView!.frame.size.width) - CGFloat(menuItems!.count) * heightCell) / 2.0
        tableView?.contentInset = UIEdgeInsetsMake(inset, 0, 0, 0)
        
        showControllerAtIndex(0)
    }
    
    // MARK: - Table View methods
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return menuItems?.count ?? 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        var cell = tableView.dequeueReusableCellWithIdentifier("MenuCell") as UITableViewCell?
        
        if (cell == nil)
        {
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "MenuCell")
        }

        let menuItem = menuItems?[indexPath.row] as Dictionary<String, String>;
        cell?.textLabel?.text = menuItem["title"]
        cell?.backgroundColor = UIColor.clearColor()
        
        return cell!
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return heightCell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        showControllerAtIndex(indexPath.row)
    }
    
    func showControllerAtIndex(index : Int)
    {
        let appDelegate = UIApplication.sharedApplication().delegate as TRAppDelegate
        let storyboad = UIStoryboard(name: "Main", bundle: nil)
        let menuItem = menuItems?[index] as Dictionary<String, String>;
        let viewController = storyboad.instantiateViewControllerWithIdentifier(menuItem["vc"]!) as UIViewController
        
        viewController.title = menuItem["title"]
        appDelegate.contentViewController = UINavigationController(rootViewController: viewController)
    }
    
    func showBook(book: NSArray)
    {
        let appDelegate = UIApplication.sharedApplication().delegate as TRAppDelegate
        let storyboad = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboad.instantiateViewControllerWithIdentifier("TRReaderVC") as TRReaderVC
        viewController.title = "title"
        viewController.book = book
        appDelegate.contentViewController = UINavigationController(rootViewController: viewController)
    }
}
