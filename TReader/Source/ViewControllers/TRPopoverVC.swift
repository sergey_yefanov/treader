//
//  TRPopoverVC.swift
//  TReader
//
//  Created by Sergey Yefanov on 15.02.15.
//  Copyright (c) 2015 Sergey Yefanov. All rights reserved.
//

import UIKit

class TRPopoverVC: UIViewController, UITableViewDataSource, UITableViewDelegate
{
    var size : CGSize = CGSizeZero
    var words : [String]? = nil
        {
            didSet
            {
                size = CGSize(width: 200, height: CGFloat(words!.count) * cellHeight)
            }
        }
    
    let cellHeight : CGFloat = 15
    
    @IBOutlet weak var tableView : UITableView?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        tableView?.separatorStyle = .None
        
        // Do any additional setup after loading the view.
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        var cell = tableView.dequeueReusableCellWithIdentifier("MenuCell") as UITableViewCell?
        
        if (cell == nil)
        {
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "MenuCell")
        }
        
        cell?.textLabel?.text = words![indexPath.row]
        cell?.textLabel?.font = UIFont(name: "Helvetica", size: 11)
        
        return cell!;
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return words?.count ?? 0
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return cellHeight
    }
}
