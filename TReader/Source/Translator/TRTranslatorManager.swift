//
//  TRTranslatorManager.swift
//  TReader
//
//  Created by Sergey Yefanov on 02.02.2015

import Foundation

let _SingletonSharedInstanceTranslator = TRTranslatorManager()

class TRTranslatorManager: NSObject
{
    // MARK: - Constants
    let localTranslator = TRLocalTranslator.sharedInstance
    let networkTranslator = TRNetworkTranslator.sharedInstance
    
    // MARK: - Variables
    
    var allWords = Dictionary <String, String>()
    var wordsForLoading = Array <String>()
    
    // MARK: - Initialization
    
    class var sharedInstance : TRTranslatorManager
    {
        return _SingletonSharedInstanceTranslator
    }
    
    // MARK: - Translation
    
    func translate(#word: String, completion: ([String]?) -> ())
    {
        if let dataFromMemory = allWords[word]
        {
            completion([dataFromMemory])
        }
        else
        {
            if let localWord = localTranslator.translate(word)
            {
                completion([localWord])
            }
            else
            {
                return networkTranslator.translate(word: word, completion)
            }
        }
    }
}


