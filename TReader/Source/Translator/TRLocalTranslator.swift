//
//  TRLocalTranslator.swift
//  TReader
//
//  Created by Sergey Yefanov on 09.02.15.
//  Copyright (c) 2015 Sergey Yefanov. All rights reserved.
//

let _singleton = TRLocalTranslator()

class TRLocalTranslator
{
    // MARK: - Variabels
    var offlineDict  = Dictionary <String, String>(minimumCapacity: 22000)
    
    // MARK: - Initialization
    
    class var sharedInstance : TRLocalTranslator
    {
        return _singleton
    }
    
    init()
    {
//        loadDictionary()
    }
    
    // MARK: - loadDictionaries
    
    func loadDictionary()
    {
        let path = NSBundle.mainBundle().pathForResource("dictionary", ofType: "json")
        let data = NSData(contentsOfFile: path!)
        var err: NSError?
        
        let jsonResult = NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers, error: &err) as NSDictionary
        let resultsDataArray = jsonResult["data"] as Array<Dictionary<String, String>>
        var resultDictionary  = Dictionary <String, String>()
        
        for var i = 0; i < resultsDataArray.count; i++
        {
            let object = resultsDataArray[i]
            
            resultDictionary[object["original"]!] = object["translation"]!
        }
        
        offlineDict = resultDictionary
    }

    // MARK: - Methods
    
    func translate(word: String) -> String?
    {
        return offlineDict[word]
    }
}