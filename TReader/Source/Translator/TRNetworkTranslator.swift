//
//  TRNetworkTranslator.swift
//  TReader
//
//  Created by Sergey Yefanov on 02.02.2015

import Foundation

private let singleton = TRNetworkTranslator()

protocol TRNetworkTranslatorDelegate {
    func didReceiveResult(results: Dictionary <String, (String, Int)>)
}

class TRNetworkTranslator
{
    // MARK: - Variabels
    
    var wordCurrent :String = ""
    var delegate: TRNetworkTranslatorDelegate?
    
    // MARK: - Initialization
    
    init()
    {
        
    }
    
    init(delegate: TRNetworkTranslatorDelegate?)
    {
        self.delegate = delegate
    }
    
    class var sharedInstance : TRNetworkTranslator
    {
        return singleton
    }
    
    // MARK: - Methods
    
    func translate(#word: String, completion: ([String]?) -> ())
    {
        TRLinguaLeoTranslator.sharedInstance.translate(word: word, completion: completion)
    }
    
    func loadTranslationFor(word: String)
    {
        
        wordCurrent = word.lowercaseString
        
        let url = NSURL(string: "https://dictionary.yandex.net/api/v1/dicservice.json/lookup?key=dict.1.1.20140629T075752Z.31afbbf86cc0e661.1dfb936ab3d20f742ee61e0d103c19d5871b83a3&lang=en-ru&text=\(word)")

        var jsonResult: NSDictionary? = [:]
        
        let cache = NSCache()

        jsonResult = cache.objectForKey(word) as? NSDictionary
    
        if jsonResult != nil
        {
            let session = NSURLSession.sharedSession()
            let task = session.dataTaskWithURL(url!, completionHandler: {data, response, error -> Void in
                if error != nil {
                    // If there is an error in the web request, print it to the console
                    println(error.localizedDescription)
                }
                var err: NSError?
                jsonResult = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: &err) as? NSDictionary
                    if (err?) != nil {
                    }
                    
                    self.parse(jsonResult!)
                
                    self.delegate?.didReceiveResult(self.parse(jsonResult!))
                })
            task.resume()
        } else {
//            self.delegate?.didReceiveResult(self.parse(jsonResult!))
        }
    }
    
    func parse(json: NSDictionary) -> Dictionary <String, (String, Int)> {
        
        var translateDict = Dictionary<String, (String, Int)>()
        
        let defs = json["def"] as NSArray
        
        if defs.count > 0 {
            let ts = defs[0] as NSDictionary
            
            var word:String = ts["text"] as String
            
            var trs = ts["tr"] as NSArray
            
            if trs.count > 0 {
                var tr = trs[0] as NSDictionary
                var translete = tr["text"] as String
            
                let randomNumber = Int(arc4random_uniform(100))
                translateDict[word.lowercaseString] = (translete, randomNumber)
            }
        }
        
        if translateDict.count == 0 {
            let randomNumber = Int(arc4random_uniform(100))

            translateDict[wordCurrent] = ("Unknown", randomNumber)
        }

        return translateDict
    }
    
    
//    func load()
//    {
//        if wordsForLoading.count > 0
//        {
//            var wordForLoading: String = wordsForLoading[0] as String
//            
//            if let obj = allWords[wordForLoading]
//            {
//                wordsForLoading.removeAtIndex(0)
//                load()
//            }
//            else
//            {
//                var loader: TRNetworkTranslator = TRNetworkTranslator(delegate: self)
//                wordsForLoading.removeAtIndex(0)
//                loader.loadTranslationFor(wordForLoading)
//            }
//        }
//        else
//        {
//            if let delegate = delegate
//            {
//                dispatch_async(dispatch_get_main_queue(),
//                    {
//                        delegate.didTranslate()
//                })
//            }
//        }
//    }
//    
//    func didReceiveResult(results: Dictionary <String, (String, Int)>) {
//        
//        for key in results.keys {
//            var object = results[key];
//            allWords[key] = object;
//            
//        }
//        
//        load()
//    }
}