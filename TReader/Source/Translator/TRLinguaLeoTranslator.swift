//
//  TRLinguaLeoTranslator.swift
//  TReader
//
//  Created by Sergey Yefanov on 15.02.15.
//  Copyright (c) 2015 Sergey Yefanov. All rights reserved.
//

import UIKit

 let _singletonLL = TRLinguaLeoTranslator()

class TRLinguaLeoTranslator: NSObject
{
    
    class var sharedInstance : TRLinguaLeoTranslator
    {
        return _singletonLL
    }
    
    func translate(#word: String, completion: ([String]?) -> ())
    {
        let manager = AFHTTPRequestOperationManager()
        
        let url = NSString(format: "http://api.lingualeo.com/gettranslates?word=%@", word)
        manager.GET(url,
            parameters: nil,
            success:
            { (operation: AFHTTPRequestOperation!, responseObject: AnyObject!) -> Void in
                
                let responseDict = responseObject as NSDictionary
                let translates = responseDict["translate"] as NSArray
                
                var values : NSMutableSet = NSMutableSet()
                
                for translate in translates
                {
                    let word = translate["value"] as String
                    values.addObject(word)
                }

                completion(values.allObjects as? [String])
            })
            
            { (operation: AFHTTPRequestOperation!, error:NSError!) -> Void in
                
            }
    }
    
    func autorize()
    {
        let urlRegister = "http://api.lingualeo.com/login"
        let data = ["email" : "sergey.yefanov@mail.ru", "password" : "zjrhUG "]
        let manager = AFHTTPRequestOperationManager()
        
        manager.POST(   urlRegister,
                        parameters: data,
                        success:
                        { (operation: AFHTTPRequestOperation!, responseObject: AnyObject!) -> Void in
                            let cookie = operation.response.allHeaderFields["Set-Cookie"] as String

                            let lingualeoidRegex = "lingualeouid=([0-9]+)"
                            let AWSELBRegex = "AWSELB=([^;]+)"
                            let firstseenRegex = "firstseen=([^;]+)"
                            let useridRegex = "userid=([0-9]+)"
                            let rememberRegex = "remember=([^;]+)"
                            let servidRegex = "servid=([^;]+)"

//                            if let match = cookie.rangeOfString(lingualeoidRegex, options: .RegularExpressionSearch)
//                            {
//                                lingualeoid = cookie.substringWithRange(match)
//                                myCookie.setValue(lingualeoid, forKey: "lingualeoid")
//                                println("lingualeoid = \(lingualeoid)")
//                            }
//
//                            if let match = cookie.rangeOfString(AWSELBRegex, options: .RegularExpressionSearch)
//                            {
//                                AWSELB = cookie.substringWithRange(match)
//                                myCookie.setValue(AWSELB, forKey: "AWSELB")
//                                println("AWSELB = \(AWSELB)")
//                            }
//
//                            if let match = cookie.rangeOfString(firstseenRegex, options: .RegularExpressionSearch)
//                            {
//                                firstseen = cookie.substringWithRange(match)
//                                myCookie.setValue(firstseen, forKey: "firstseen")
//                                println("firstseen = \(firstseen)")
//                            }
//
//                            if let match = cookie.rangeOfString(useridRegex, options: .RegularExpressionSearch)
//                            {
//                                userid = cookie.substringWithRange(match)
//                                myCookie.setValue(userid, forKey: "userid")
//                                println("userid = \(userid)")
//                            }
//
//                            if let match = cookie.rangeOfString(rememberRegex, options: .RegularExpressionSearch)
//                            {
//                                remember = cookie.substringWithRange(match)
//                                myCookie.setValue(remember, forKey: "remember")
//                                println("remember = \(remember)")
//                            }
//
//                            if let match = cookie.rangeOfString(servidRegex, options: .RegularExpressionSearch)
//                            {
//                                servid = cookie.substringWithRange(match)
//                                myCookie.setValue(servid, forKey: "servid")
//                                println("servid = \(servid)")
//                            }
                        })
            
                        { (operation: AFHTTPRequestOperation!, error:NSError!) -> Void in
            
                        }
    }
}
