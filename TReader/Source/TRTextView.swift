//
//  TRTextView.swift
//  TReader
//
//  Created by Sergey Yefanov on 02.02.2015

import UIKit

protocol TRTextViewDelegate{
    func didTap(#word: String?, point: CGPoint?)
}

class TRTextView: UIView
{
	// MARK: - Properties
    
    var textAttributes = Dictionary<String, AnyObject>()
    var translationLabels = Array<UILabel>()
    var originalTextView : UITextView!
    var delegate: protocol <TRTextViewDelegate>?
    var text: String = String()
    {
        willSet
        {
            originalTextView.text = newValue
        }
    }
    
    // MARK: - Initialize
    
    required init(coder aDecoder: (NSCoder!))
    {
        super.init(coder: aDecoder)
        initialize()
	}
	
	func initialize()
    {
        // UITextView
		originalTextView = UITextView()
        originalTextView.editable = false
        originalTextView.setTranslatesAutoresizingMaskIntoConstraints(false)
		addSubview(originalTextView)
        
        let paragraphStyle = NSMutableParagraphStyle()
        let lineHeight: CGFloat = 50
        paragraphStyle.lineHeightMultiple = lineHeight
        paragraphStyle.maximumLineHeight = lineHeight
        paragraphStyle.minimumLineHeight = lineHeight
        paragraphStyle.alignment = NSTextAlignment.Justified
        textAttributes = [NSParagraphStyleAttributeName : paragraphStyle]
        
        // Constraints
        let metrics = ["hMargin" : 20, "topMargin" : 20]
        let views = ["originalTextView" : originalTextView]
        addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-hMargin-[originalTextView]-hMargin-|",
            options: NSLayoutFormatOptions.DirectionLeadingToTrailing, metrics: metrics, views: views))
        addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-topMargin-[originalTextView]|",
            options: NSLayoutFormatOptions.DirectionLeadingToTrailing, metrics: metrics, views: views))
        
		let selector: Selector = "singleTapRecognized:"
		let singleTap = UITapGestureRecognizer(target: self, action: selector)
		singleTap.numberOfTapsRequired = 1
		originalTextView.addGestureRecognizer(singleTap)
	}
	
    // MARK: - Setters
	
	func setText(text: String) {
		self.text = text
        
        let attributedText = NSMutableAttributedString(string: text, attributes: textAttributes)
        attributedText.addAttribute(NSFontAttributeName, value: UIFont(name: "Bangla Sangam MN", size: 20)!, range: NSMakeRange(0, attributedText.length))
        originalTextView.attributedText = attributedText
	}
    
    func setDictionary(dictionary: Dictionary<String, String>) {
        
        for subview in translationLabels {
            subview.removeFromSuperview()
        }
        translationLabels.removeAll(keepCapacity: false)
        
        for (word, translation) in dictionary {
            
            for rect in getAllRectsByWord(word) {
                
                let translationLabel = UILabel()
                translationLabel.font = UIFont(name:"Verdana", size:12)
                translationLabel.text = translation
                translationLabel.textAlignment = NSTextAlignment.Center
                translationLabel.textColor = UIColor(red: 0/255, green: 7/255, blue: 255/255, alpha: 1)
                
                let newRect = CGRectOffset(rect, CGRectGetWidth(rect) * 0.5, -8)
                translationLabel.sizeToFit()
                let width = CGRectGetWidth(translationLabel.frame)
                translationLabel.frame = CGRectMake(CGRectGetMinX(newRect) - width * 0.5, CGRectGetMinY(newRect), width, CGRectGetHeight(newRect))
                
                translationLabels.append(translationLabel)
                originalTextView.addSubview(translationLabel)
            }
        }
    }
	
    // MARK: - Word Methods
	
	func getWordAtPosition(var pos: CGPoint, textView: UITextView) -> String?
    {
		let tapPos = textView.closestPositionToPoint(pos)
		let range = textView.tokenizer.rangeEnclosingPosition(tapPos, withGranularity: UITextGranularity.Word, inDirection: UITextDirection())
        
        return range != nil ? textView.textInRange(range!) : nil
	}
	
	func getAllRectsByWord(word: String) -> Array<CGRect> {
		
        let locations = getAllWordLocationsOf(word, inText: originalTextView.text)
        
		var rects = Array<CGRect>()

		for value in locations {
			
			let beginning = self.originalTextView.beginningOfDocument
            
			let start = self.originalTextView.positionFromPosition(beginning, offset:value.location)
			let end = self.originalTextView.positionFromPosition(start!, offset:value.length)
            
			let textRange = self.originalTextView.textRangeFromPosition(start, toPosition:end)
			let rect = self.originalTextView.firstRectForRange(textRange)
            
			rects.append(rect)
		}
        
		return rects
	}

	func getAllWordLocationsOf(word: String, inText text: String) -> Array<NSRange>
    {
        var locations = Array<NSRange>()
		
        var searchRange = Range(start: 0, end: countElements(text) - 1)
        var wordRange = Range(start: 0, end: countElements(word) - 1)
		
//		while searchRange.startIndex < countElements(text)
//        {
//			searchRange.length = countElements(text) - searchRange.location
//            wordRange = text.rangeOfString(word, options: NSStringCompareOptions.LiteralSearch, range: <#Range<String.Index>?#>, locale: <#NSLocale?#>)
//			wordRange = text.rangeOfString(word, options:NSStringCompareOptions.LiteralSearch, range:searchRange)
//            
//            if wordRange.location == NSNotFound {
//                break
//            } else {
//                searchRange.location = wordRange.location + wordRange.length
//                
//                let nextSymbol = text.substringWithRange(Range(start: searchRange.location, end: searchRange.location + 1))
//                let prevSymbol = text.substringWithRange(NSMakeRange(wordRange.location - 1, 1))
//                
//                if !isLetter(prevSymbol) && !isLetter(nextSymbol) {
//                    locations.append(wordRange)
//                }
//			}
//		}
        
		return locations
	}
	
    func isLetter(letterStringOpt: String!) -> Bool {
        if let letterString = letterStringOpt {
            switch letterString {
            case "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z":
                return true
            default:
                return false
            }
        }
        return false
    }
    
	func singleTapRecognized(gestureRecognizer: UIGestureRecognizer) {
		var word = getWordAtPosition(gestureRecognizer.locationInView(originalTextView), textView: originalTextView)
        
        if var word = word
        {
            word = word.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
            
            if !word.isEmpty {
                delegate?.didTap(word: word, point: gestureRecognizer.locationInView(self))
            }
        }
	}
}
