//
//  AppDelegate.swift
//  TReader
//
//  Created by Sergey Yefanov on 02.02.2015

import UIKit

@UIApplicationMain
public class TRAppDelegate: UIResponder, UIApplicationDelegate {
                            
    public var window: UIWindow?
    public var contentViewController : UIViewController?
    {
        didSet
        {
            sideMenuViewController?.setContentViewController(contentViewController, animated: true)
            sideMenuViewController?.hideMenuViewController()
        }
    }
    
    var menuViewController : TRMenuVC?
    {
        let storyboad = UIStoryboard(name: "Main", bundle: nil)
        return storyboad.instantiateViewControllerWithIdentifier("TRMenuVC") as? TRMenuVC
    }
    
    var sideMenuViewController : RESideMenu?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: NSDictionary?) -> Bool
    {
        sideMenuViewController = RESideMenu(contentViewController: UIViewController(),
                                           leftMenuViewController: menuViewController,
                                          rightMenuViewController: nil)

        self.window?.rootViewController = sideMenuViewController;
        return true
    }
    
    public func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject?) -> Bool
    {
        TRFileManager.sharedInstance.addItem(path: url)
        return true
    }
}